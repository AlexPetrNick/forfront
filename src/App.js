import {useEffect} from "react";
import Chart from "react-apexcharts";
import useWebSocket from "react-use-websocket";
import {useState} from "react";
import {useTimer} from "react-timer-hook";

export default function App() {
    const getAccess = () => {
        const header = new Headers();
        header.append("Content-Type", "application/json")
        fetch(`${process.env.BACK_URL}/api/token`, {
            mode: "cors",
            withCredentials: true,
            headers: header,
            method: "POST",
            body: JSON.stringify({
                "username": "system",
                "password": "Qwerty123!"
            })
        }).then(response => response.json())
            .then(data => setStateToken(data["access"]))
    }

    const [stateToken, setStateToken] = useState("")
    const [statusGame, setStatusGame] = useState("run")
    const [win, setWin] = useState(0)
    const [tableS, setTable] = useState([
        1000, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001,
        1000, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001,
        1000, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001, 1001
    ])
    const [data, updateData] = useState([1000]);
    const [socketUrl, setSocketUrl] = useState('ws://localhost:8000/ws/quotation/graph')

    const {lastMessage} = useWebSocket(`${socketUrl}?token=${stateToken}`)

    useEffect(() => {
        getAccess()
        setInterval(getAccess, 1000000);
    }, [])

    useEffect(() => {
        if (lastMessage !== null) {
            const mes = JSON.parse(lastMessage.data)
            if ("value" in mes) {
                updateData([...data, mes["value"]])
            }
            if ("status_game" in mes) {
                setStatusGame(mes["status_game"])
            }
            if ("table" in mes) {
                setTable(mes["table"].map(a=>a["value"]))
            }
            if ("quotation_win" in mes) {
                setWin(mes["quotation_win"]["value"])
            }
        }
    }, [lastMessage]);


    const cssStyle = {
        border: "1px solid black",
        width: 60,
        height: 30,
        display: "flex",
        "align-items": "center",
        "justify-content": "center",
    }

    const cssStyleWin = {
        border: "1px solid black",
        width: 60,
        height: 30,
        display: "flex",
        "align-items": "center",
        "justify-content": "center",
        "background-color": "#179900"

    }

    const flexBox = {
        display: "flex",
        width: 800,
        "flex-wrap": "wrap"
    }

    function MyTimer({ expiryTimestamp }) {
        const {
            seconds,
            minutes,
            hours,
            days,
            isRunning,
            start,
            pause,
            resume,
            restart,
        } = useTimer({ expiryTimestamp, onExpire: () => console.warn('onExpire called') });


        return (
            <div style={{textAlign: 'center'}}>
                <div style={{fontSize: '100px'}}>
                    <span>{days}</span>:<span>{hours}</span>:<span>{minutes}</span>:<span>{seconds}</span>
                </div>
                <button onClick={start}>{expiryTimestamp}</button>
                <button onClick={pause}>Pause</button>
                <button onClick={resume}>Resume</button>
                <button onClick={() => {
                    restart(expiryTimestamp)
                }}>Restart</button>
            </div>
        );
    }

    return (
        <div className="App">
            <div style={{display: "flex", width: 900, "justify-content": "space-around"}}>
                <h1 style={{width: 350}}>Status: {statusGame}</h1>
                <h1 style={{width: 300}}>Winning: {win}</h1>
            </div>
            <ApexChart data={data} title="Product Trends by Month"/>
            <h2 style={{width: 730, "text-align": "center"}}>Table</h2>
            <div style={{display: "flex"}}>
                <div style={flexBox}>
                    {tableS.map(a => {
                        const match = a === win
                        return (
                            match ? <div style={cssStyleWin}>{a}</div> : <div style={cssStyle}>{a}</div>
                        )
                    })}
                </div>
            </div>
            <MyTimer expiryTimestamp={1000}/>
        </div>
    );
}

function ApexChart(props) {
    const series = [
        {
            name: "xx",
            data: props.data
        }
    ];
    const options = {
        chart: {
            stacked: true,
            parentHeightOffset: 1
        },
        yaxis: {
            show: true,
            showAlways: true,
            min: 0,
            max: 2000
        },
        xaxis: {
            type: "numeric",
            range: 500
        },
        grid: {
            show: true
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: 2,
            curve: "smooth",
            show: true
        },
        colors: ["#210124"],
        fill: {
            type: "gradient",
            gradient: {
                shadeIntensity: 1,
                inverseColors: true,
                gradientToColors: ["#DB162F"],
                opacityFrom: 1,
                opacityTo: 1,
                type: "vertical",
                stops: [0, 30]
            }
        }
    }


    return (
        <div id="chart">
            <Chart options={options} series={series} type="line" height={450}/>
        </div>
    );
}